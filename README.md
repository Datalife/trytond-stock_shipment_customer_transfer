datalife_stock_shipment_customer_transfer
=========================================

The stock_shipment_customer_transfer module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-stock_shipment_customer_transfer/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-stock_shipment_customer_transfer)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
